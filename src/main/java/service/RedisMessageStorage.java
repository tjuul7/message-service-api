package service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import exception.NoMessagesInPositionException;
import exception.UnauthorizedException;
import main.MessageService;
import model.MessageRecord;
import model.MessageStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.time.ZonedDateTime.now;

/**
 * Message Storage that connects to Redis
 *
 * Start evt. Redis: docker run -p 6379:6379 --name my-redis-container -d redis
 *
 * @author Thomas Anderse, Thomas Juul
 */
public class RedisMessageStorage implements MessageStorage
{
  private static final Logger logger = LoggerFactory.getLogger(RedisMessageStorage.class);

  private final Gson clGson;
  private JedisPool clJedisPool;

  public RedisMessageStorage(String host, int port)
  {
    clGson = new Gson();
    createJedisPool(host, port);
  }

  /**
   * Constructor
   */
  public RedisMessageStorage()
  {
    clGson = new Gson();
    createJedisPool("localhost", 6379);
  }

  public boolean bIsHealthy()
  {
    boolean bAbleToGetResource = false;
    try (Jedis jedis = clJedisPool.getResource())
    {
      jedis.ping(); //Ping the Jedis to check if it's alive! If not it will throw the exception
      return true;
    }
    catch ( JedisConnectionException e)
    {
      return false;
    }
  }

  /**
   * Get the Redis key used for Wall postings
   * @param sPosition
   * @return
   */
  private String sGetRedisWallKey( String sPosition ) { return "WallRoom" + sPosition; }

  /**
   * Creates the Jedis Pool with Host and Port
   * @param sHost
   * @param nPort
   */
  public void createJedisPool( String sHost, int nPort)
  {
    clJedisPool = new JedisPool(sHost,nPort);
  }


  /**
   * Adds a Message to the Wall
   * @param positionInCave the position of the room in the cave
   * @param messageRecord  the message to add to the 'wall'
   */
  @Override
  public MessageRecord addMessage(String positionInCave, MessageRecord messageRecord)
  {
    try (Jedis jedis = clJedisPool.getResource())
    {
      //Get all MessageRecords from Redis
      String sCurrentWallMessages = jedis.get( sGetRedisWallKey(positionInCave) );
      List<MessageRecord> clMessagesInRoom = new ArrayList<>();
      if( sCurrentWallMessages != null )
      {
        //JSon the sh*t out of them.
        clMessagesInRoom = clGson.fromJson( sCurrentWallMessages, new TypeToken<List<MessageRecord>>() {}.getType() );
      }

      //Add new messageRecord in the beginning of the list.
      messageRecord.setCreatorTimeStampISO8601(now());
      messageRecord.setId(UUID.randomUUID().toString());
      clMessagesInRoom.add(0, messageRecord);

      String sUpdatedWallMessages = clGson.toJson(clMessagesInRoom);
      String sStatusCodeReply = jedis.set( sGetRedisWallKey( positionInCave), sUpdatedWallMessages);
      logger.info("Added msg {} to Redis storage", sUpdatedWallMessages);
    }

    // need to return the message because the newly created id should be part of location returned
    return messageRecord;
  }

  /**
   * Updates a message at the wall
   * @param positionInCave   the (x,y,z) position in the cave of the room that contains the message to update
   * @param messageId        the unique id of the message to update
   * @param newMessageRecord a messagerecord that must include the playerID of the player wanting to change the wall
   *                         message, as well as the new contents.
   * @return
   */
  @Override
  public MessageRecord updateMessage(String positionInCave, String messageId, MessageRecord newMessageRecord) throws Exception
  {
    List<MessageRecord> clMessagesInRoom = new ArrayList<>();
    try (Jedis jedis = clJedisPool.getResource())
    {
      //Get all MessageRecords from Redis
      String sCurrentWallMessages = jedis.get(sGetRedisWallKey(positionInCave));
      if (sCurrentWallMessages != null)
      {
        //JSon the sh*t out of them.
        clMessagesInRoom = clGson.fromJson(sCurrentWallMessages, new TypeToken<List<MessageRecord>>(){}.getType());
      }
    }

    // Search and find index of first message with same ID as updating message
    int nLocationOfMessage = -1;
    for(int i=0; i<clMessagesInRoom.size(); i++)
    {
      if( clMessagesInRoom.get(i).getId().equals(messageId ) )
      {
        nLocationOfMessage = i;
        break;
      }
    }

    // Bail out if no message with given id exists
    if( nLocationOfMessage == -1 )
    {
      throw new NoMessagesInPositionException("No messages found");
    }

    // Bail out if the found message was not created by same person
    if (! clMessagesInRoom.get(nLocationOfMessage).getCreatorId().equals(newMessageRecord.getCreatorId()))
    {
      throw new UnauthorizedException(String.format("CreatorId not the same. Existing: %s and requested: %s", clMessagesInRoom.get(nLocationOfMessage).getCreatorId(), newMessageRecord.getCreatorId()));
    }

    newMessageRecord.setId(messageId);
    newMessageRecord.setCreatorTimeStampISO8601(now());

    // Update message and enter it back into the list
    clMessagesInRoom.get(nLocationOfMessage).setContents(newMessageRecord.getContents());

    try (Jedis jedis = clJedisPool.getResource())
    {
      String sUpdatedWallMessages = clGson.toJson(clMessagesInRoom);
      String sStatusCodeReply = jedis.set(sGetRedisWallKey(positionInCave), sUpdatedWallMessages);

      logger.info("Updated msg {} in Redis storage", sUpdatedWallMessages);
    }

    return newMessageRecord;
  }

  /**
   * Gets the Message List from a Position
   * @param positionInCave the position of the room in the cave
   * @param startIndex     absolute index of message to start the page
   * @param pageSize       getNumber of messages to retrieve
   * @return
   */
  @Override
  public List<MessageRecord> getMessageList(String positionInCave, int startIndex, int pageSize)
  {
    List<MessageRecord> clMessagesInRoom = new ArrayList<>();

    try (Jedis jedis = clJedisPool.getResource())
    {
      //Get all MessageRecords from Redis
      String sCurrentWallMessages = jedis.get(sGetRedisWallKey(positionInCave));
      if (sCurrentWallMessages != null)
      {
        //JSon the sh*t out of them.
        clMessagesInRoom = clGson.fromJson(sCurrentWallMessages, new TypeToken<List<MessageRecord>>(){}.getType());
      }
    }    int size = clMessagesInRoom.size();
    int firstIndex = startIndex > size ? size : startIndex;
    int lastIndex = startIndex + pageSize > size ? size : startIndex + pageSize;
    return clMessagesInRoom.subList(firstIndex, lastIndex);
  }
}
