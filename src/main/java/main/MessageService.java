package main;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import doubles.FakeMessageStorage;
import exception.*;
import model.MessageRecord;
import model.MessageStorage;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.RedisMessageStorage;
import spark.Request;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.*;

import static spark.Spark.get;
import static spark.Spark.post;

public class MessageService {
    private static final Logger logger = LoggerFactory.getLogger(MessageService.class);
    public static final String APP_AND_VERSION_PATH = "/msdo/v1";

    private static MessageStorage storage;
    private static boolean bStopHammerTime = false;

    private static void initialize(String[] args) {
        String realStorageHost = null;

        if (args.length > 0){
            realStorageHost = args[0];
        }

        if (realStorageHost != null) {
            String realStoragePort = null;

            if (args.length == 2) {
                realStoragePort = args[1];
            }

            int port = 6379;    // default redis port

            if (realStoragePort != null) {
                port = Integer.parseInt(realStoragePort);
            }

            logger.info("LogType=Startup Storage=Redis Host={} Port={}", realStorageHost, port);
            storage = new RedisMessageStorage(realStorageHost, port);
        }
        else
        {
            logger.info("LogType=Startup Storage=FakeMessageStorage");
            storage = new FakeMessageStorage();
        }
    }

    public static void main(String[] args) {
        initialize(args);

        // add new message
        post(APP_AND_VERSION_PATH + "/message/:positionString", (request, response) -> {
            response.type("application/json");

            String positionString = request.params(":positionString");
            MessageRecord messageRecord = null;

            try
            {
                validateQueryParms(request, true, false, false, false);
                messageRecord = new Gson().fromJson(request.body(), MessageRecord.class);
                validateBody(messageRecord);
            }
            catch (MissingQueryParameterException | MissingFieldException e)
            {
                response.status(HttpStatus.BAD_REQUEST_400);
                logger.info("LogType=AddNewMessage HTTPStatus=" + response.raw().getStatus() );
                return new Gson().toJson("");
            }

            messageRecord = storage.addMessage(positionString, messageRecord);

            // *** response part ***

            // http response header -> Location: /message/{positionInCave}/{id}
            String location = String.format("%s/message/%s/%s", APP_AND_VERSION_PATH, positionString, messageRecord.getId());
            response.status(HttpStatus.CREATED_201);
            response.header("Location", location);

            logger.info("LogType=AddNewMessage HTTPStatus=" + response.raw().getStatus() + " Location=" + location);

            return new Gson().toJson(messageRecord);
        });

        // update message
        post(APP_AND_VERSION_PATH + "/message/:positionString/:id", (request, response) -> {
            response.type("application/json");

            MessageRecord messageRecord = null;

            String positionString = request.params(":positionString");
            String id = request.params(":id");

            try
            {
                validateQueryParms(request, true, true, false, false);
                messageRecord = new Gson().fromJson(request.body(), MessageRecord.class);
                validateBody(messageRecord);
            }
            catch (MissingQueryParameterException | MissingFieldException e)
            {
                response.status(HttpStatus.BAD_REQUEST_400);
                logger.info("LogType=UpdateMessage HTTPStatus=" + response.raw().getStatus() );
                return new Gson().toJson("");
            }

            MessageRecord updatedMessageRecord = null;

            try
            {
                updatedMessageRecord = storage.updateMessage(positionString, id, messageRecord);
            }
            catch (NoMessagesInPositionException | NoMessageFoundException e)
            {
                response.status(HttpStatus.NOT_FOUND_404);
                logger.info("LogType=UpdateMessage HTTPStatus=" + response.raw().getStatus() );
                return new Gson().toJson("");
            }
            catch (UnauthorizedException e)
            {
                response.status(HttpStatus.UNAUTHORIZED_401);
                logger.info("LogType=UpdateMessage HTTPStatus=" + response.raw().getStatus() );
                return new Gson().toJson("");
            }

            // http response header -> Location: /message/{positionInCave}/{id}
            String location = String.format("%s/message/%s/%s", APP_AND_VERSION_PATH, positionString, id);
            response.header("Location", location);
            logger.info("LogType=UpdateMessage HTTPStatus=" + response.raw().getStatus() + " Location=" + location);

            return new Gson().toJson(updatedMessageRecord);
        });

        // Get message list
        get(APP_AND_VERSION_PATH + "/message/:positionString/:startIndex/:pageSize", (request, response) -> {
            response.type("application/json");

            String positionString = request.params(":positionString");
            String sStartIndex = request.params(":startIndex");
            String sPageSize = request.params(":pageSize");

            try
            {
                validateQueryParms(request, true, false, true, true);
            }
            catch (MissingQueryParameterException e)
            {
                response.status(HttpStatus.BAD_REQUEST_400);
                logger.info("LogType=GetMessageList HTTPStatus=" + response.raw().getStatus() );
                return new Gson().toJson("");
            }

            int startIndex = Integer.parseInt(sStartIndex);
            int pageSize = Integer.parseInt(sPageSize);

            List<MessageRecord> messageList = storage.getMessageList(positionString, startIndex, pageSize);

            logger.info(messageList.toString());

            response.status(HttpStatus.OK_200);
            logger.info("LogType=GetMessageList HTTPStatus=" + response.raw().getStatus() );
            return new Gson().toJson(messageList);
        });

        get("/health", (request, response) ->
        {
            //According to Nygard a Health page should return the following:
            // - IP address (or addresses)
            // - Version number of the runtime (Java 11)
            // - Application version or commit ID (APP_AND_VERSION_PATH)
            // - Is the instance accepting work
            // - Status of connection ports, caches and circuit breakers (connection to redis)

            String sIpAddress = "Unknown";
            try(final DatagramSocket socket = new DatagramSocket()){
                socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                sIpAddress = socket.getLocalAddress().getHostAddress();
            }

            String sVersion = System.getProperty("java.version");

            String sStorageType;
            if( storage.getClass() == RedisMessageStorage.class )
                sStorageType = "RedisMessageStorage";
            else
                sStorageType = "FakeMessageStorage";

            String sHealthState = storage.bIsHealthy() && !bStopHammerTime ? "Healthy" : "Unhealthy";

            String sJson  = "{";
            sJson += "\"IP Address\":\"" + sIpAddress + "\"";
            sJson += ",\"Java Version\":\"" + sVersion + "\"";
            sJson += ",\"API Version\":\"" + APP_AND_VERSION_PATH + "\"";
            sJson += ",\"Database\":\"" + sStorageType + "\"";
            sJson += ",\"Status\":\"" + sHealthState + "\"";
            sJson += "}";

            response.type("application/json");
            response.status(HttpStatus.OK_200);

            logger.info("LogType=HealthRequestRespond HTTPStatus=" + response.raw().getStatus() + " " + sParseJsonToLogger( sJson ) );

            return sJson;
        });

        get("/stop", (request, response) ->
        {
            bStopHammerTime = true;
            //response.type("application/html");
            response.status(HttpStatus.OK_200);
            return "<H1>HAMMERTIME</H1>";
        });
    }

    private static String sParseJsonToLogger( String sJson )
    {
        String sReturn = sJson;
        //UnJson the stuff so it's ready for LOG
        sReturn = sReturn.replace("{", "");
        sReturn = sReturn.replace("}", "");
        sReturn = sReturn.replace("\"", "");
        sReturn = sReturn.replace(":", "=");
        sReturn = sReturn.replace(",", " ");
        return sReturn;
    }

    /**
     * Validate body contents
     *
     * @param messageRecord
     * @throws MissingFieldException
     */
    private static void validateBody(MessageRecord messageRecord) throws MissingFieldException {
        if (Strings.isNullOrEmpty(messageRecord.getContents())) {
            logger.error("Bad request -> contents field not filled out");
            throw new MissingFieldException("contents field not filled out");
        }

        if (Strings.isNullOrEmpty(messageRecord.getCreatorId())) {
            logger.error("Bad request -> creatorId field not filled out");
            throw new MissingFieldException("creatorId field not filled out");
        }

        if (Strings.isNullOrEmpty(messageRecord.getCreatorName())) {
            logger.error("Bad request -> creatorName field not filled out");
            throw new MissingFieldException("creatorName field not filled out");
        }
    }

    /**
     * Validate if required request parameters exists
     *
     * @param request
     * @param validatePositionString
     * @throws MissingQueryParameterException
     */
    private static void validateQueryParms(Request request, boolean validatePositionString, boolean validateId, boolean validateStartIndex, boolean validatePageSize) throws MissingQueryParameterException {
        if (validatePositionString) {
            String positionString = request.params(":positionString");
            if (positionString == null) {
                logger.error("Bad request -> positionString request query parameter missing");
                throw new MissingQueryParameterException("positionString request query parameter missing");
            }
        }

        if (validateId) {
            String id = request.params(":id");
            if (id == null) {
                logger.error("Bad request -> id request query parameter missing");
                throw new MissingQueryParameterException("id request query parameter missing");
            }
        }

        if (validateStartIndex) {
            String startIndex = request.params(":startIndex");
            if (startIndex == null) {
                logger.error("Bad request -> startIndex request query parameter missing");
                throw new MissingQueryParameterException("startIndex request query parameter missing");
            }

            try {
                Integer.parseInt(startIndex);
            } catch (NumberFormatException nfe) {
                logger.error("Bad request -> startIndex request query parameter is not a number");
                throw new MissingQueryParameterException("startIndex request query parameter is not a number");
            }
        }

        if (validatePageSize) {
            String pageSize = request.params(":pageSize");
            if (pageSize == null) {
                logger.error("Bad request -> pageSize request query parameter missing");
                throw new MissingQueryParameterException("pageSize request query parameter missing");
            }

            try {
                Integer.parseInt(pageSize);
            } catch (NumberFormatException nfe) {
                logger.error("Bad request -> pageSize request query parameter is not a number");
                throw new MissingQueryParameterException("pageSize request query parameter is not a number");
            }
        }

    }
}
