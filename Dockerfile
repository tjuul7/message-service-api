# Usage: docker build --no-cache -t tjuul7/message-service .
# -------------
# Run with default (Fake storage):
#
# docker run -d -p 4567:4567 --name delta-message-service tjuul7/message-service
#
# Run with Redis storage:
# docker run -p 6379:6379 --name my-redis-container -d redis
# docker run -d -p 4567:4567 --name delta-message-service tjuul7/message-service java -jar /root/message-service/messageservice.jar ip-address 6379


# Use java 11 gradle 6.8 from Bærbak
FROM henrikbaerbak/jdk11-gradle68 AS builder

# Set the working directory to /root/cave and copy what's needed
WORKDIR /root/message-service
COPY build.gradle /root/message-service
COPY settings.gradle /root/message-service
COPY src /root/message-service/src

# Build the fatjar deployment file
RUN gradle :fatJar

# Create new container
FROM henrikbaerbak/jdk11-gradle68

LABEL Author="Thomas Andersen"
LABEL Author="Thomas Juul"
LABEL Group="Delta"

# Copy the necessary jar file
WORKDIR /root/message-service
COPY --from=builder /root/message-service/build/libs/messageservice.jar /root/message-service/messageservice.jar

# Expose port so clients can connect
EXPOSE 4567

CMD ["java", "-jar", "/root/message-service/messageservice.jar"]
